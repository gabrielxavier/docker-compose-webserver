### Docker Compose - WebServer:

- HaProxy 1.8 with SSL and Health Checks
- Nginx 1.14 and PHP 7.2
- Redis 4.0 for sessions and cache
- Mariadb 10.3 with Persistent Storage

        Webports: 80, 443
        HealthCheck: /stats
