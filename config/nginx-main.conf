daemon off;
user nginx;
worker_processes auto;
pid /run/nginx.pid;

error_log /dev/stdout info;

worker_rlimit_nofile 1024;

events {
    worker_connections 1024;
    # multi_accept on;
}

http {
    access_log /dev/stdout;

    log_format  main    '$remote_addr - $remote_user [$time_local] "$request" '
                        '$status $body_bytes_sent "$http_referer" '
                        '"$http_user_agent" "$http_x_forwarded_for"';

    open_file_cache max=1024 inactive=10s;
    open_file_cache_valid 120s;

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;
    include /etc/nginx/mime.types;
    default_type application/octet-stream;
    types {
        font/woff2 woff2;
    }
    client_max_body_size 32M;
    
    real_ip_header X-Forwarded-For;
    server_tokens off;

    include /etc/nginx/conf.d/*.conf;

## Enable Gzip compression
    gzip on;

    # Disable Gzip on IE6.
    gzip_disable "msie6";

    # Allow proxies to cache both compressed and regular version of file.
    # Avoids clients that don't support Gzip outputting gibberish.
    gzip_vary on;

    # Compress data, even when the client connects through a proxy.
    gzip_proxied any;

    # The level of compression to apply to files. A higher compression level increases
    # CPU usage. Level 5 is a happy medium resulting in roughly 75% compression.
    gzip_comp_level 5;

    # The minimum HTTP version of a request to perform compression.
    gzip_http_version 1.1;

    # Don't compress files smaller than 256 bytes, as size reduction will be negligible.
    gzip_min_length 256;

    # Compress the following MIME types.
    gzip_types
        application/atom+xml
        application/javascript
        application/json
        application/ld+json
        application/manifest+json
        application/rss+xml
        application/vnd.geo+json
        application/vnd.ms-fontobject
        application/x-font-ttf
        application/x-web-app-manifest+json
        application/xhtml+xml
        application/xml
        font/opentype
        image/bmp
        image/svg+xml
        image/x-icon
        text/cache-manifest
        text/css
        text/plain
        text/vcard
        text/vnd.rim.location.xloc
        text/vtt
        text/x-component
        text/x-cross-domain-policy;

}
